import numpy as np

class Container(object):
    '''
    Base Material Object
    It contains several materail properties
    '''

    def __init__(self):
        self.children       = []

    def append(self, object):
        self.children.append(object)

    def overall_cg(self):

        massCg          = 0
        mass            = 0

        for element in self.children:

            massCg      += element.cg * element.density * element.V
            mass        += element.density * element.V

        self.cg         = massCg / mass
        self.mass       = mass

    def overall_inertia(self):
        pass

    def overall(self):
        pass

    def report(self):
        pass

class Tensor(Container):
    '''
    A Tensor is an object, whose inertias are known
    Such inertia tensor is rotated to global navigation if needed
    '''
    def __init__(self):

        super(Tensor, self).__init__()

        self.phi        = 0
        self.theta      = 0
        self.psi        = 0

        self.I          = np.array([[0., 0., 0.],
                                    [0., 0., 0.],
                                    [0., 0., 0.]])

    def rotation(self):

        self.A2B        = np.array([[np.cos(self.theta) * np.cos(self.psi),
                                         np.sin(self.phi) * np.sin(self.theta) * np.cos(self.psi) - np.cos(self.phi) * np.sin(self.psi),
                                         np.cos(self.phi) * np.sin(self.theta) * np.cos(self.psi) + np.sin(self.phi) * np.sin(self.psi)],

                                        [np.cos(self.theta) * np.sin(self.psi),
                                         np.sin(self.phi) * np.sin(self.theta) * np.sin(self.psi) + np.cos(self.phi) * np.cos(self.psi),
                                         np.cos(self.phi) * np.sin(self.theta) * np.sin(self.psi) + np.sin(self.phi) * np.cos(self.psi)],

                                        [- np.sin(self.theta),
                                         np.sin(self.phi) * np.cos(self.theta),
                                         np.cos(self.phi) * np.cos(self.theta)]])

        self.A2BT       = np.transpose(self.A2B)

        self.I          = np.dot(np.dot(self.A2B, self.I), self.A2BT)

class Part(Container):

    def __init__(self):

        super(Part, self).__init__()
        self.material       = None

    # Construct inherited class methods
    # Part could be either Shell or Solid
    @classmethod
    def virtual_constructor(cls, type, *args, **kwargs):

        obj             = cls()

        if type == 'Shell':
            obj.__class__   = Shell
            obj.__init__()

        if type == 'Solid':
            obj.__class__   = Solid
            obj.__init__()

        return obj

    @classmethod
    def get_child(cls):

        obj             = cls()

class Shell(Part):

    def __init__(self):

        super(Shell, self).__init__()

        self.Ixx        = 0
        self.Iyy        = 0
        self.Izz        = 0
        self.Ixy        = 0
        self.Ixz        = 0
        self.Iyz        = 0

    # Construct inherited class methods
    # Polymorph to parent class virtual constructor
    # Shell could be either Triangle or Quad
    @classmethod
    def virtual_constructor(cls, type, *args, **kwargs):

        obj             = cls()

        if type == 'Triangle':
            obj.__class__   = Triangle
            obj.__init__()

        if type == 'Quad':
            obj.__class__   = Quad
            obj.__init__()

        return obj

    def discretized_geometry(self, fileName, type):

        with open(fileName, 'r') as input:

            inputLines      = input.readlines()
            inputWords      = [inputLines[i].split() for i in range(len(inputLines))]

        nNode               = int(inputWords[0][0])
        nElem               = int(inputWords[nNode + 1][0])

        nodes               = []

        for i in range(1, nNode + 1):

            node            = Node()
            node.points     = np.array([float(inputWords[i][0]), float(inputWords[i][1]), float(inputWords[i][2])])
            nodes.append(node)

        self.children            = []

        for i in range(nElem):

            elemType        = 3
            elem            = []

            for j in range(elemType):
                correspondingNode       = nodes[int(inputWords[i + nNode + 2][j]) - 1]
                elem.append(correspondingNode)
            element         = self.virtual_constructor(type)
            element.density = 1000
            element.nodes   = elem
            element.vertex()
            element.volume(thickness=1e-3)
            element.center()
            self.children.append(element)

    def calculate_cg(self):

        massCg          = 0
        mass            = 0

        for element in self.children:

            massCg      += element.cg * element.density * element.V
            mass        += element.density * element.V

        self.cg         = massCg / mass
        self.mass       = mass

    def calculate_inertia(self):

        for element in self.children:

            delta               = element.cg - self.cg

            self.Ixx            += (delta[1] * delta[1] + delta[2] * delta[2]) * element.density * element.V
            self.Iyy            += (delta[0] * delta[0] + delta[2] * delta[2]) * element.density * element.V
            self.Izz            += (delta[0] * delta[0] + delta[1] * delta[1]) * element.density * element.V

            self.Ixy            += (delta[0] * delta[1]) * element.density * element.V
            self.Ixz            += (delta[0] * delta[2]) * element.density * element.V
            self.Iyz            += (delta[1] * delta[2]) * element.density * element.V

        self.I                  = np.array([[self.Ixx, self.Ixy, self.Ixz],
                                            [self.Ixy, self.Iyy, self.Iyz],
                                            [self.Ixz, self.Iyz, self.Izz]])

class Solid(Part):

    def __init__(self):

        super(Solid, self).__init__()

        self.Ixx        = 0
        self.Iyy        = 0
        self.Izz        = 0
        self.Ixy        = 0
        self.Ixz        = 0
        self.Iyz        = 0

    # Construct inherited class methods
    # Polymorph to parent class virtual constructor
    # Shell could be either Tetrahedral or Prism or Hex
    @classmethod
    def virtual_constructor(cls, type, *args, **kwargs):

        obj             = cls()

        if type == 'Tetrahedral':
            obj.__class__   = Tetrahedral
            obj.__init__()

        if type == 'Prism':
            obj.__class__   = Prism
            obj.__init__()

        if type == 'Hex':
            obj.__class__   = Hex
            obj.__init__()

        return obj

    def material(self, type):
        pass

    def discretized_geometry(self, fileName, type):


        with open(fileName, 'r') as input:

            inputLines          = input.readlines()
            inputWords          = [inputLines[i].split() for i in range(len(inputLines))]

        nodeStartIndex          = 0
        elemStartIndex          = 0

        for i in range(len(inputLines)):

            if inputWords[i][0] == '$Nodes':
                nodeStartIndex  = i + 1

            if inputWords[i][0] == '$Elements':
                elemStartIndex  = i + 1
                break

        nNode           = int(inputWords[nodeStartIndex][0])
        nElem           = int(inputWords[elemStartIndex][0])

        self.nodes      = []

        for i in range(nNode):

            node        = Node()

            node.points = np.array(
                [float(inputWords[i + nodeStartIndex + 1][1]), float(inputWords[i + nodeStartIndex + 1][2]),
                 float(inputWords[i + nodeStartIndex + 1][3])])

            self.nodes.append(node)

        self.children       = []

        for i in range(nElem):

            elemType        = int(inputWords[i + elemStartIndex + 1][1])
            container       = []
            if elemType < 4:
                break
            for j in range(elemType):
                correspondingNode = self.nodes[int(inputWords[i + elemStartIndex + 1][5 + j]) - 1]
                container.append(correspondingNode)
            element             = self.virtual_constructor(type)
            element.nodes       = container
            element.density     = 1000

            element.vertex()
            element.volume()
            element.center()

            self.children.append(element)

    def calculate_cg(self):

        massCg          = 0
        mass            = 0

        for element in self.children:

            massCg      += element.cg * element.density * element.V
            mass        += element.density * element.V

        self.cg         = massCg / mass
        self.mass       = mass

    def calculate_inertia(self):

        for element in self.children:

            delta               = element.cg - self.cg

            self.Ixx            += (delta[1] * delta[1] + delta[2] * delta[2]) * element.density * element.V
            self.Iyy            += (delta[0] * delta[0] + delta[2] * delta[2]) * element.density * element.V
            self.Izz            += (delta[0] * delta[0] + delta[1] * delta[1]) * element.density * element.V

            self.Ixy            += (delta[0] * delta[1]) * element.density * element.V
            self.Ixz            += (delta[0] * delta[2]) * element.density * element.V
            self.Iyz            += (delta[1] * delta[2]) * element.density * element.V

        self.I                  = np.array([[self.Ixx, -self.Ixy, -self.Ixz],
                                            [-self.Ixy, self.Iyy, -self.Iyz],
                                            [-self.Ixz, -self.Iyz, self.Izz]])

class Triangle(Shell):

    def __init__(self):

        super(Triangle, self).__init__()

        self.type           = 3
        self.nodes          = None
        self.cg             = np.array([0., 0., 0.])
        self.density        = None
        self.ID             = None
        self.A              = None
        self.V              = None

    def vertex(self):

        self.vertices       = []

        for i in range(1, self.type):
            vertex          = self.nodes[i].points - self.nodes[0].points
            self.vertices.append(vertex)

    def volume(self, thickness):

        self.A              = np.linalg.norm(np.cross(self.vertices[0], self.vertices[1])) / 2.
        self.V              = self.A * thickness

    def center(self):

        for i in range(self.type):

            self.cg         += self.nodes[i].points

        self.cg             /= self.type

class Quad(Shell):

    def __init__(self):

        super(Quad, self).__init__()

class Tetrahedral(Solid):

    def __init__(self):

        super(Tetrahedral, self).__init__()

        self.type           = 4                     # nNodes
        self.nodes          = None
        self.cg             = np.array([0., 0., 0.])
        self.density        = None
        self.ID             = None
        self.A              = None
        self.V              = None

    def vertex(self):

        self.vertices       = []

        for i in range(1, self.type):
            vertex          = self.nodes[i].points - self.nodes[0].points
            self.vertices.append(vertex)

    def volume(self):

        self.V              = np.dot(np.cross(self.vertices[0], self.vertices[1]), self.vertices[2]) / 6.

    def center(self):

        for i in range(self.type):

            self.cg         += self.nodes[i].points

        self.cg             /= self.type

class Prism(Solid):

    def __init__(self):

        super(Prism, self).__init__()

class Hex(Solid):

    def __init__(self):

        super(Hex, self).__init__()

class Node(object):

    def __init__(self):

        self.points         = None
        self.ID             = None

container       = Container()

partList        = [['Composite', 'Shell', 'Triangle', 'SSD_case\\Part3_shell_solid_mix\\body_shell.dat'],
                   ['Alluminium', 'Solid', 'Tetrahedral', 'SSD_case\\Part3_shell_solid_mix\\wing_tail_blocks.msh']]

for material, feature, geometry, name in partList:
    part            = Part()
    part            = part.virtual_constructor(feature)
    part.discretized_geometry(name, geometry)
    part.calculate_cg()
    part.calculate_inertia()

    container.append(part)